package xronbo.ronbopvp.utils;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;
import xronbo.ronbopvp.RonboPVP;

public class Utils {

    static Plugin plugin = RonboPVP.get();

    static final Material[] tornadoMats = {
            Material.DIRT,
            Material.GRASS,
            Material.STONE,
            Material.COBBLESTONE,
            Material.COAL_ORE,
            Material.COAL_BLOCK,
            Material.REDSTONE_ORE,
            Material.GOLD_ORE,
            Material.MOSSY_COBBLESTONE
    };

    public static void createTornado(final Location l, Player p2) {
        final String name = p2.getName();
        RonboPVP.get().getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            public int count = 16;
            public void run()  {
                for(int k = 0; k < 20; k++) {
                    final Location loc = l.clone();
                    plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                        public void run() {
                            if(fb == null) {
                                fb = loc.getWorld().spawnFallingBlock(loc, tornadoMats[(int)(Math.random() * tornadoMats.length)], (byte)0);
                                fb.setDropItem(false);
                            }
                            double radius = Math.sin(verticalTicker()) * 2;
                            float horisontal = horisontalTicker();
                            Vector v = new Vector(radius * Math.cos(horisontal), 0.15D, radius * Math.sin(horisontal));
                            fb.setVelocity(v);
                            for(Entity e : fb.getNearbyEntities(0.5, 0.5, 0.5))
                                if(e instanceof Player) {
                                    Player p = (Player)e;
                                    if(p.getName().equals(name))
                                        continue;
                                    RonboPVP.get().getGame().lastAttackedBy.put(p.getName(), name);
                                    if(p.hasMetadata("exited")) {
                                        p.damage(1.5);
                                        p.setVelocity(p.getVelocity().setY(0.2));
                                        p.setFallDistance(0);
                                    }
                                }
                            if(count-- > 0)
                                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, this, 1);
                            else
                                fb.remove();
                        }
                        public FallingBlock fb = null;
                        public int count = 60;
                        private float ticker_vertical = 0.0f;
                        private float ticker_horisontal = (float) (Math.random() * 2 * Math.PI);
                        public float horisontalTicker() {
                            return (ticker_horisontal += 0.8f);
                        }
                        private float verticalTicker() {
                            if (ticker_vertical < 1.0f) {
                                ticker_vertical += 0.05f;
                            }
                            return ticker_vertical;
                        }
                    }, k * (int)(Math.random() * 3));
                }
                if(count-- > 0)
                    plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, this, 10);
            }
        });
    }

    public static void createShockwave(Player p2) {
        final String name = p2.getName();
        final Location loc = p2.getLocation();
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            public int count = 2;
            public void run() {
                for(int dx = -count; dx <= count; dx++) {
                    int dz = count;
                    if(Math.random() < 0.5)
                        continue;
                    Location temp = loc.clone().add(dx,0,dz);
                    Material m = Material.DIRT;
                    if(temp.getWorld().getBlockAt(temp.clone().add(0, -1, 0)).getType() != Material.AIR)
                        m = temp.getWorld().getBlockAt(temp.clone().add(0, -1, 0)).getType();
                    final FallingBlock fb = loc.getWorld().spawnFallingBlock(temp, m, (byte)0);
                    fb.setDropItem(false);
                    fb.setVelocity(new Vector(0,Math.random() * 0.3 + 0.3,0));
                    plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                        public int count = 7;
                        public void run() {
                            for(Entity e : fb.getNearbyEntities(0.5, 0.5, 0.5)) {
                                if(e instanceof Player) {
                                    Player p = (Player)e;
                                    if(p.getName().equals(name))
                                        continue;
                                    RonboPVP.get().getGame().lastAttackedBy.put(p.getName(), name);
                                    if(p.hasMetadata("exited")) {
                                        p.damage(4);
                                        p.setVelocity(new Vector(0,0.4,0));
                                    }
                                }
                            }
                            if(count-- > 0)
                                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, this, 2);
                            else
                                fb.remove();
                        }
                    });
                }
                for(int dx = -count; dx <= count; dx++) {
                    int dz = -count;
                    if(Math.random() < 0.5)
                        continue;
                    Location temp = loc.clone().add(dx,0,dz);
                    Material m = Material.DIRT;
                    if(temp.getWorld().getBlockAt(temp.clone().add(0, -1, 0)).getType() != Material.AIR)
                        m = temp.getWorld().getBlockAt(temp.clone().add(0, -1, 0)).getType();
                    final FallingBlock fb = loc.getWorld().spawnFallingBlock(temp, m, (byte)0);
                    fb.setDropItem(false);
                    fb.setVelocity(new Vector(0,Math.random() * 0.3 + 0.3,0));
                    plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                        public int count = 7;
                        public void run() {
                            for(Entity e : fb.getNearbyEntities(0.5, 0.5, 0.5)) {
                                if(e instanceof Player) {
                                    Player p = (Player)e;
                                    if(p.getName().equals(name))
                                        continue;
                                    RonboPVP.get().getGame().lastAttackedBy.put(p.getName(), name);
                                    if(p.hasMetadata("exited")) {
                                        p.damage(2);
                                        p.setVelocity(new Vector(0,0.4,0));
                                    }
                                }
                            }
                            if(count-- > 0)
                                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, this, 2);
                            else
                                fb.remove();
                        }
                    });
                }
                for(int dz = -count + 1; dz <= count - 1; dz++) {
                    int dx = -count;
                    if(Math.random() < 0.5)
                        continue;
                    Location temp = loc.clone().add(dx,0,dz);
                    Material m = Material.DIRT;
                    if(temp.getWorld().getBlockAt(temp.clone().add(0, -1, 0)).getType() != Material.AIR)
                        m = temp.getWorld().getBlockAt(temp.clone().add(0, -1, 0)).getType();
                    final FallingBlock fb = loc.getWorld().spawnFallingBlock(temp, m, (byte)0);
                    fb.setDropItem(false);
                    fb.setVelocity(new Vector(0,Math.random() * 0.3 + 0.3,0));
                    plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                        public int count = 7;
                        public void run() {
                            for(Entity e : fb.getNearbyEntities(0.5, 0.5, 0.5)) {
                                if(e instanceof Player) {
                                    Player p = (Player)e;
                                    if(p.getName().equals(name))
                                        continue;
                                    RonboPVP.get().getGame().lastAttackedBy.put(p.getName(), name);
                                    if(p.hasMetadata("exited")) {
                                        p.damage(2);
                                        p.setVelocity(new Vector(0,0.4,0));
                                    }
                                }
                            }
                            if(count-- > 0)
                                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, this, 2);
                            else
                                fb.remove();
                        }
                    });
                }
                for(int dz = -count + 1; dz <= count - 1; dz++) {
                    int dx = count;
                    if(Math.random() < 0.5)
                        continue;
                    Location temp = loc.clone().add(dx,0,dz);
                    Material m = Material.DIRT;
                    if(temp.getWorld().getBlockAt(temp.clone().add(0, -1, 0)).getType() != Material.AIR)
                        m = temp.getWorld().getBlockAt(temp.clone().add(0, -1, 0)).getType();
                    final FallingBlock fb = loc.getWorld().spawnFallingBlock(temp, m, (byte)0);
                    fb.setDropItem(false);
                    fb.setVelocity(new Vector(0,Math.random() * 0.3 + 0.3,0));
                    plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                        public int count = 7;
                        public void run() {
                            for(Entity e : fb.getNearbyEntities(0.5, 0.5, 0.5)) {
                                if(e instanceof Player) {
                                    Player p = (Player)e;
                                    if(p.getName().equals(name))
                                        continue;
                                    RonboPVP.get().getGame().lastAttackedBy.put(p.getName(), name);
                                    if(p.hasMetadata("exited")) {
                                        p.damage(2);
                                        p.setVelocity(new Vector(0,0.4,0));
                                    }
                                }
                            }
                            if(count-- > 0)
                                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, this, 2);
                            else
                                fb.remove();
                        }
                    });
                }
                if(count++ < 8)
                    plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, this, 3);
            }
        });
    }

    public static void removeMetadata(Player p) {
        p.removeMetadata("exited", plugin);
        p.removeMetadata("arrowblock", plugin);
        p.removeMetadata("firespirit", plugin);
        p.removeMetadata("thunderspirit", plugin);
        p.removeMetadata("earthspirit", plugin);
        p.removeMetadata("kastiaspirit", plugin);
        p.removeMetadata("windspirit", plugin);
        p.removeMetadata("supercaster", plugin);
        p.removeMetadata("gravity", plugin);
        p.removeMetadata("repulsion", plugin);
        p.removeMetadata("relentless", plugin);
        p.removeMetadata("unstoppable", plugin);
    }
}
