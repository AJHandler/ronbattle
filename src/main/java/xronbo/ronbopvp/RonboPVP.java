package xronbo.ronbopvp;

import net.kastia.KastiaCore.database.Callback;
import net.kastia.KastiaCore.database.Database;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import xronbo.ronbopvp.arena.ArenaManager;
import xronbo.ronbopvp.command.PVPCommand;
import xronbo.ronbopvp.kits.SelectionListener;
import xronbo.ronbopvp.mechanics.PVPGame;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

public class RonboPVP extends JavaPlugin implements Listener {

	//Main Plugin Instance
	private static RonboPVP instance;

	private PVPGame game;

	private ArenaManager arenaManager;

	//Cached Stats
	private ConcurrentHashMap<String, Stats> stats = new ConcurrentHashMap<>();

	public void onEnable() {
		//Init some instance variables
		this.instance = this;

		//Register arena manager, game listener and kit selection listener
		Bukkit.getPluginManager().registerEvents(arenaManager = new ArenaManager(Bukkit.getWorld("world")), this);
		Bukkit.getPluginManager().registerEvents(game = new PVPGame(), this);
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getPluginManager().registerEvents(new SelectionListener(), this);

		//Register the commands
		PVPCommand pvpCommand = new PVPCommand();
		getCommand("db").setExecutor(pvpCommand);
		getCommand("unlock").setExecutor(pvpCommand);
		getCommand("stats").setExecutor(pvpCommand);
	}


	public void onDisable() {
		stats.values().forEach(Stats::save);

	}

	public ArenaManager getArenaManager() {
		return arenaManager;
	}

	public PVPGame getGame() {
		return game;
	}

	public ConcurrentHashMap<String, Stats> getStats() {
		return stats;
	}

	public static RonboPVP get() {
		return instance;
	}

	public static class Stats {
		public int kills = 0;
		public int deaths = 0;
		public int earnedPoints = 0;
		public int points = 0;
		public int longest = 0;
		public String joinDate = null;
		public final String uuid;
		public final String player;
		public boolean disabled = false;
		public HashSet<String> kitunlocks = new HashSet<>();
		public boolean loaded = false;

		public int getTotalPoints() {
			return earnedPoints + points;
		}

		public static final SimpleDateFormat dateformatCST;

		static {
			dateformatCST = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss zzz");
			dateformatCST.setTimeZone(TimeZone.getTimeZone("CST"));
		}

		public void save() {
			if (!loaded)
				return;
			String date = dateformatCST.format(new Date());
			if (joinDate == null) {
				joinDate = date;
			}
			StringBuilder sb = new StringBuilder();
			for (String s : kitunlocks)
				sb.append(s + " ");
			Database.get().asyncSyncInsert("insert into playerdata(uuid, kitkills, kitdeaths, kitpoints, kitlongest,  kitunlocks) values ('" + uuid + "', " + kills +
					", " + deaths + ", " + getTotalPoints() + ", " + longest + ", '" + sb.toString().trim() + "') on duplicate key update kitkills = " + kills + ", kitdeaths = " + deaths +
					", kitpoints = kitpoints + " + earnedPoints + ", kitlongest = " + longest + ", kitunlocks = '" + sb.toString().trim() + "'", null);
			points = points + earnedPoints;
			earnedPoints = 0;
		}

		private void load() {
			Bukkit.getServer().getScheduler().runTaskAsynchronously(get(), () -> {
                try {
                    Database.get().asyncSyncQuery("select kitkills, kitdeaths, kitpoints, kitlongest, joinDate, kitunlocks from playerdata where uuid = '" + uuid + "'"
                            , null, new Callback() {
                                @Override
                                public void read(ResultSet rs) throws SQLException {
                                    loaded = true;
                                    if (rs.next()) {
                                        kills = rs.getInt("kitkills");
                                        deaths = rs.getInt("kitdeaths");
                                        points = rs.getInt("kitpoints");
                                        longest = rs.getInt("kitlongest");
                                        joinDate = rs.getString("joinDate");
                                        if (rs.getString("kitunlocks") != null)
                                            kitunlocks.addAll(Arrays.asList(rs.getString("kitunlocks").split(" ")));
                                    } else {
                                        save();
                                    }
                                }

                                @Override
                                public void digestAsync() {

                                }

                                @Override
                                public void digestSync() {

                                }

                                @Override
                                public Object result() {
                                    return null;
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
		}

		public Stats(String player, String uuid) {
			this.player = player;
			this.uuid = uuid;
			load();
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(get(), () -> {
                if (disabled)
                    return;
                save();
            }, 20 * 120);
		}
	}

	@EventHandler
	public void quit(PlayerQuitEvent event) {
		if (stats.containsKey(event.getPlayer().getName())) {
			stats.get(event.getPlayer().getName()).save();
			stats.get(event.getPlayer().getName()).disabled = true;
			stats.remove(event.getPlayer());
		}
	}

	public void loadPlayer(final Player p) {
		final String uuid = p.getUniqueId().toString();
		p.teleport(Bukkit.getServer().getWorld("world").getSpawnLocation().add((int) (Math.random() * 7 - 3), 0, (int) (Math.random() * 7 - 3)));
		p.getInventory().clear();
		p.setScoreboard(Bukkit.getServer().getScoreboardManager().getMainScoreboard());
		p.sendMessage(ChatColor.GREEN + "");
		stats.put(p.getName(), new Stats(p.getName(), uuid));
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		loadPlayer(event.getPlayer());
		Player p = event.getPlayer();
		stats.put(p.getName(), new Stats(p.getName(), p.getUniqueId().toString()));
		event.setJoinMessage(null);
	}

	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent event) {
		event.setQuitMessage("");
	}
}
