package xronbo.ronbopvp.kits;

import com.google.common.collect.Lists;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import xronbo.ronbopvp.RonboPVP;

import java.util.Arrays;
import java.util.List;

public class TankKit extends AbstractKit {

    public TankKit() {
        super("Tank");
    }

    @Override
    public void onKillStreak(int ks, Player p, RonboPVP.Stats stats) {
        switch (ks) {
            case 4:
                p.sendMessage(ChatColor.AQUA + "Tank " + ks + " Kill Streak Reward Earned: Exercise");
                p.removePotionEffect(PotionEffectType.SLOW);
                p.addPotionEffect(PotionEffectType.SLOW.createEffect(200000, 0));
                break;
            case 9:
                p.sendMessage(ChatColor.AQUA + "Tank " + ks + " Kill Streak Reward Earned: Beefy");
                p.setMaxHealth(60.0);
                p.setHealth(60.0);
                break;
            case 19:
                p.sendMessage(ChatColor.AQUA + "Tank " + ks + " Kill Streak Reward Earned: Gravity");
                p.setMetadata("gravity", new FixedMetadataValue(plugin, 1));
                final String name = p.getName();
                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    public void run() {
                        if (plugin.getServer().getPlayerExact(name) == null || !plugin.getServer().getPlayerExact(name).hasMetadata("gravity"))
                            return;
                        plugin.getServer().getPlayerExact(name).getNearbyEntities(10, 5, 10).stream().filter(e -> e instanceof Player).forEach(e -> ((Player) e).addPotionEffect(PotionEffectType.SLOW.createEffect(20 * 5, 1)));
                        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, this, 20 * 2);
                    }
                });
                break;
            case 25:
                if (stats.kitunlocks.contains("repulsion")) {
                    p.sendMessage(ChatColor.AQUA + "Tank " + ks + " Kill Streak Reward Earned: Repulsion");
                    p.setMetadata("repulsion", new FixedMetadataValue(plugin, 0));
                }
                break;
            default:
                break;
        }
    }

    public List<ItemStack> getKitItems() {
        List<ItemStack> kit = Lists.newArrayList();
        ItemStack item = new ItemStack(Material.DIAMOND_HELMET);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_PROJECTILE, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_FIRE, 2);
        kit.add(item);
        item = new ItemStack(Material.DIAMOND_CHESTPLATE);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_PROJECTILE, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_FIRE, 2);
        kit.add(item);
        item = new ItemStack(Material.DIAMOND_LEGGINGS);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_PROJECTILE, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_FIRE, 2);
        kit.add(item);
        item = new ItemStack(Material.DIAMOND_BOOTS);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_PROJECTILE, 2);
        item.addUnsafeEnchantment(Enchantment.PROTECTION_FIRE, 2);
        kit.add(item);
        item = new ItemStack(Material.WOOD_SWORD);
        ItemMeta im = item.getItemMeta();
        im.setDisplayName(ChatColor.AQUA + "Tank Sword");
        im.setLore(Arrays.asList(ChatColor.YELLOW + "Doesn't do much."));
        item.setItemMeta(im);
        kit.add(item);
        Potion potion = new Potion(PotionType.INSTANT_HEAL);
        potion.setLevel(2);
        kit.add(potion.toItemStack(5));
        item = new ItemStack(Material.COOKED_BEEF);
        item.setAmount(64);
        kit.add(item);
        return kit;
    }
}
