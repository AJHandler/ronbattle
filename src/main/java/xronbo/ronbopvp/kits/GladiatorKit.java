package xronbo.ronbopvp.kits;

import com.google.common.collect.Lists;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import xronbo.ronbopvp.RonboPVP;

import java.util.Arrays;
import java.util.List;

public class GladiatorKit extends AbstractKit {

    public GladiatorKit() {
        super("Gladiator");
    }

    @Override
    public void onKillStreak(int ks, Player p, RonboPVP.Stats stats) {
        switch (ks) {
            case 3:
                p.sendMessage(ChatColor.AQUA + "Gladiator " + ks + " Kill Streak Reward Earned: Relentless");
                p.setMetadata("relentless", new FixedMetadataValue(plugin, 0));
                break;
            case 7:
                p.sendMessage(ChatColor.AQUA + "Gladiator " + ks + " Kill Streak Reward Earned: Bloodthirsty");
                ItemStack item = new ItemStack(Material.COMPASS);
                ItemMeta im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "The Hunter");
                im.setLore(Arrays.asList(new String[]{ChatColor.YELLOW + "Right click to cast spell!",}));
                item.setItemMeta(im);
                p.getInventory().addItem(item);
                break;
            case 15:
                p.sendMessage(ChatColor.AQUA + "Gladiator " + ks + " Kill Streak Reward Earned: Unstoppable");
                p.setMetadata("relentless", new FixedMetadataValue(plugin, 0));
                break;
            case 25:
                if (stats.kitunlocks.contains("duelist")) {
                    p.sendMessage(ChatColor.AQUA + "Gladiator " + ks + " Kill Streak Reward Earned: Duelist");
                    p.addPotionEffect(PotionEffectType.REGENERATION.createEffect(200000, 2));
                }
                break;
            default:
                break;
        }
    }

    public List<ItemStack> getKitItems() {
        List<ItemStack> kit = Lists.newArrayList();
        ItemStack item = new ItemStack(Material.LEATHER_HELMET);
        ItemMeta im = item.getItemMeta();
        ((LeatherArmorMeta)im).setColor(Color.ORANGE);
        item.setItemMeta(im);
        item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        kit.add(item);
        item = new ItemStack(Material.LEATHER_CHESTPLATE);
        im = item.getItemMeta();
        ((LeatherArmorMeta)im).setColor(Color.ORANGE);
        item.setItemMeta(im);
        item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        kit.add(item);
        item = new ItemStack(Material.LEATHER_LEGGINGS);
        im = item.getItemMeta();
        ((LeatherArmorMeta)im).setColor(Color.ORANGE);
        item.setItemMeta(im);
        item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        kit.add(item);
        item = new ItemStack(Material.LEATHER_BOOTS);
        im = item.getItemMeta();
        ((LeatherArmorMeta)im).setColor(Color.ORANGE);
        item.setItemMeta(im);
        item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
        kit.add(item);
        item = new ItemStack(Material.DIAMOND_SWORD);
        item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 3);
        im = item.getItemMeta();
        im.setDisplayName(ChatColor.GOLD + "Gladiator's Blade");
        im.setLore(Arrays.asList(ChatColor.YELLOW + "Real sharp!"));
        item.setItemMeta(im);
        kit.add(item);
        Potion potion = new Potion(PotionType.INSTANT_HEAL);
        potion.setLevel(2);
        kit.add(potion.toItemStack(5));
        item = new ItemStack(Material.COOKED_BEEF);
        item.setAmount(64);
        kit.add(item);
        return kit;
    }
}
