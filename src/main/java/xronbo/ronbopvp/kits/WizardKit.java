package xronbo.ronbopvp.kits;

import com.google.common.collect.Lists;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;
import xronbo.ronbopvp.RonboPVP;

import java.util.Arrays;
import java.util.List;

public class WizardKit extends AbstractKit {

    public WizardKit() {
        super("Wizard");
    }

    @Override
    public void onKillStreak(int ks, Player p, RonboPVP.Stats stats) {
        switch (ks) {
            case 3:
                p.sendMessage(ChatColor.AQUA + "Wizard " + ks + " Kill Streak Reward Earned: Melee Wizard");
                p.getInventory().addItem(new ItemStack(Material.IRON_SWORD));
                break;
            case 7:
                p.sendMessage(ChatColor.AQUA + "Wizard " + ks + " Kill Streak Reward Earned: Backup Plan");
                p.getInventory().addItem(new ItemStack(Material.ENDER_PEARL, 10));
                break;
            case 13:
                p.sendMessage(ChatColor.AQUA + "Wizard " + ks + " Kill Streak Reward Earned: Shockwave");
                ItemStack item = new ItemStack(Material.STICK);
                ItemMeta im = item.getItemMeta();
                im.setDisplayName(ChatColor.LIGHT_PURPLE + "Shockwave");
                im.setLore(Arrays.asList(new String[]{ChatColor.YELLOW + "Right click to cast spell!",}));
                item.setItemMeta(im);
                item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
                p.getInventory().addItem(item);
                break;
            case 25:
                if (stats.kitunlocks.contains("supercaster")) {
                    p.sendMessage(ChatColor.AQUA + "Wizard " + ks + " Kill Streak Reward Earned: Super Caster");
                    p.setMetadata("supercaster", new FixedMetadataValue(plugin, 0));
                }
                break;
            default:
                break;
        }
    }

    public List<ItemStack> getKitItems() {
        List<ItemStack> kit = Lists.newArrayList();
        ItemStack item = new ItemStack(Material.LEATHER_HELMET);
        ItemMeta im = item.getItemMeta();
        ((LeatherArmorMeta)im).setColor(Color.PURPLE);
        item.setItemMeta(im);
        item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        kit.add(item);
        item = new ItemStack(Material.LEATHER_CHESTPLATE);
        im = item.getItemMeta();
        ((LeatherArmorMeta)im).setColor(Color.PURPLE);
        item.setItemMeta(im);
        item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        kit.add(item);
        item = new ItemStack(Material.LEATHER_LEGGINGS);
        im = item.getItemMeta();
        ((LeatherArmorMeta)im).setColor(Color.PURPLE);
        item.setItemMeta(im);
        item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        kit.add(item);
        item = new ItemStack(Material.LEATHER_BOOTS);
        im = item.getItemMeta();
        ((LeatherArmorMeta)im).setColor(Color.PURPLE);
        item.setItemMeta(im);
        item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 2);
        kit.add(item);
        item = new ItemStack(Material.STICK);
        im = item.getItemMeta();
        im.setDisplayName(ChatColor.LIGHT_PURPLE + "Tornado");
        im.setLore(Arrays.asList(ChatColor.YELLOW + "Right click to cast spell!"));
        item.setItemMeta(im);
        item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
        kit.add(item);
        item = new ItemStack(Material.STICK);
        im = item.getItemMeta();
        im.setDisplayName(ChatColor.LIGHT_PURPLE + "Teleporter");
        im.setLore(Arrays.asList(ChatColor.YELLOW + "Right click to cast spell!"));
        item.setItemMeta(im);
        item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
        kit.add(item);
        Potion potion = new Potion(PotionType.INSTANT_HEAL);
        potion.setLevel(2);
        kit.add(potion.toItemStack(5));
        item = new ItemStack(Material.COOKED_BEEF);
        item.setAmount(64);
        kit.add(item);
        item = new ItemStack(Material.WOOD_SWORD);
        im = item.getItemMeta();
        im.setDisplayName(ChatColor.LIGHT_PURPLE + "Noob Sword");
        im.setLore(Arrays.asList(ChatColor.YELLOW + "Because you know.. yer a wizard."));
        item.setItemMeta(im);
        kit.add(item);
        return kit;
    }
}
