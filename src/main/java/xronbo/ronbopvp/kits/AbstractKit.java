package xronbo.ronbopvp.kits;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import xronbo.ronbopvp.RonboPVP;

import java.util.List;

public abstract class AbstractKit {

    protected RonboPVP plugin = RonboPVP.get();

    protected String name;

    public AbstractKit(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract void onKillStreak(int ks, Player p, RonboPVP.Stats stats);

    public abstract List<ItemStack> getKitItems();
}
