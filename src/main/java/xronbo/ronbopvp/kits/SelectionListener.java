package xronbo.ronbopvp.kits;

import net.kastia.KastiaCore.ranks.RankManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.SpawnEgg;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import xronbo.ronbopvp.RonboPVP;
import xronbo.ronbopvp.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectionListener implements Listener {

    @EventHandler
    public void onEntityDamageByPlayer(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player && event.getEntity() instanceof LivingEntity && event.getEntity().getCustomName() != null) {
            showKSRewards(ChatColor.stripColor(event.getEntity().getCustomName()), (Player) event.getDamager());
            event.getEntity().setFireTicks(0);
        }
    }

    @EventHandler
    public void onEntityInteract(PlayerInteractEntityEvent event) {
        Player p = event.getPlayer();
        Entity e = event.getRightClicked();
        if (e instanceof LivingEntity) {
            LivingEntity le = (LivingEntity) e;
            if (le.getCustomName() != null) {
                checkGiveKit(p, le.getCustomName().toLowerCase());
            }
        }
    }

    @EventHandler
    public void onrspn(PlayerRespawnEvent event) {
        if (event.getPlayer().hasMetadata("class")) {
            final String name = event.getPlayer().getName();
            Bukkit.getScheduler().scheduleSyncDelayedTask(RonboPVP.get(), () -> {
                Player p = Bukkit.getPlayerExact(name);
                if (p != null)
                    checkGiveKit(p, p.getMetadata("class").get(0).asString());
            });
        }
    }

    public void showKSRewards(String s, Player p) {
        Inventory inventory = Bukkit.createInventory(p, 54, ChatColor.BLACK + s + " Streak Rewards");

        ItemStack item = null;
        ItemMeta im = null;
        Potion potion = null;
        RonboPVP.Stats stats = RonboPVP.get().getStats().get(p.getName());

        item = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.BLACK.getData());
        im = item.getItemMeta();
        im.setDisplayName(ChatColor.BLACK + "");
        item.setItemMeta(im);
        for (int k = 0; k < 9; k++)
            inventory.setItem(k, item);

        item = new ItemStack(Material.BOOK);
        im = item.getItemMeta();
        im.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + s + " Kill Streak Rewards");
        im.setLore(Arrays.asList(ChatColor.YELLOW + "These Kill Streak rewards are only available when playing as a " + s + ".", "Unlock all the Kill Streak Rewards with Battle Points for maximum awesomeness!"));
        item.setItemMeta(im);
        inventory.setItem(4, item);

        item = new ItemStack(Material.IRON_FENCE);
        im = item.getItemMeta();
        im.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "vvv Locked vvv");
        im.setLore(Arrays.asList(ChatColor.YELLOW + "Unlock the below Kill Streak Rewards with Battle Points!", "Once you unlock one, it will be available forever."));
        item.setItemMeta(im);
        for (int k = 27; k < 27 + 9; k++)
            inventory.setItem(k, item);
        ArrayList<ItemStack> unlocked = new ArrayList<>();
        ArrayList<ItemStack> locked = new ArrayList<>();
        switch (s) { //s should be the NPC name, not the metadata "class" name
            case "Mercenary":
                potion = new Potion(PotionType.INSTANT_HEAL);
                item = potion.toItemStack(1);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "2 Kills - Well Supplied");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Get 20 additional Health Potions!"));
                item.setItemMeta(im);
                unlocked.add(item);

                item = new ItemStack(Material.BLAZE_ROD);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "5 Kills - Lightning Rod");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Receive a Lightning Rod that can smite your enemies!"));
                item.setItemMeta(im);
                unlocked.add(item);

                item = new ItemStack(Material.TNT);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "9 Kills - Explosive Rain");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Place the TNT to call in an storm of Explosive Rain!", "Careful, you can hurt yourself!"));
                item.setItemMeta(im);
                unlocked.add(item);

                item = new ItemStack(Material.MONSTER_EGG);
                item.setData(new SpawnEgg(EntityType.WOLF));
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "13 Kills - Wolf Pack");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Summon a pack of loyal wolves!"));
                item.setItemMeta(im);
                if (stats.kitunlocks.contains("wolfpack")) {
                    unlocked.add(item);
                } else {
                    im.setDisplayName(ChatColor.RED + ChatColor.stripColor(im.getDisplayName()) + " (LOCKED)");
                    List<String> lore = im.getLore();
                    lore.add(0, ChatColor.GOLD + "Click to unlock for 50 BP!");
                    im.setLore(lore);
                    item.setItemMeta(im);
                    locked.add(item);
                }

                item = new ItemStack(Material.MONSTER_EGG);
                item.setData(new SpawnEgg(EntityType.WOLF));
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "40 Kills - Super Wolf Pack");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Summon a pack of Super Wolfies!"));
                item.setItemMeta(im);
                if (stats.kitunlocks.contains("superwolfpack")) {
                    unlocked.add(item);
                } else {
                    im.setDisplayName(ChatColor.RED + ChatColor.stripColor(im.getDisplayName()) + " (LOCKED)");
                    List<String> lore = im.getLore();
                    lore.add(0, ChatColor.GOLD + "Click to unlock for 500 BP!");
                    im.setLore(lore);
                    item.setItemMeta(im);
                    locked.add(item);
                }
                break;
            case "Archer":
                item = new ItemStack(Material.ARROW);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "5 Kills - Fire Spirit");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "You shoot fire arrows for 2 minutes."));
                item.setItemMeta(im);
                unlocked.add(item);

                item = new ItemStack(Material.BLAZE_ROD);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "10 Kills - Thunder Spirit");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "You shoot thunder arrows for 2 minutes."));
                item.setItemMeta(im);
                unlocked.add(item);

                item = new ItemStack(Material.TNT);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "15 Kills - Earth Spirit");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "You shoot explosive arrows for 2 minutes."));
                item.setItemMeta(im);
                if (stats.kitunlocks.contains("earthspirit")) {
                    unlocked.add(item);
                } else {
                    im.setDisplayName(ChatColor.RED + ChatColor.stripColor(im.getDisplayName()) + " (LOCKED)");
                    List<String> lore = im.getLore();
                    lore.add(0, ChatColor.GOLD + "Click to unlock for 75 BP!");
                    im.setLore(lore);
                    item.setItemMeta(im);
                    locked.add(item);
                }

                item = new ItemStack(Material.BLAZE_POWDER);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "25 Kills - Spirit of Kastia");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "You shoot explosive fire arrows until you die!"));
                item.setItemMeta(im);
                if (stats.kitunlocks.contains("kastiaspirit")) {
                    unlocked.add(item);
                } else {
                    im.setDisplayName(ChatColor.RED + ChatColor.stripColor(im.getDisplayName()) + " (LOCKED)");
                    List<String> lore = im.getLore();
                    lore.add(0, ChatColor.GOLD + "Click to unlock for 300 BP!");
                    im.setLore(lore);
                    item.setItemMeta(im);
                    locked.add(item);
                }

                item = new ItemStack(Material.FEATHER);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "35 Kills - Wind Spirit");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Double Jump cooldown halved!"));
                item.setItemMeta(im);
                if (stats.kitunlocks.contains("windspirit")) {
                    unlocked.add(item);
                } else {
                    im.setDisplayName(ChatColor.RED + ChatColor.stripColor(im.getDisplayName()) + " (LOCKED)");
                    List<String> lore = im.getLore();
                    lore.add(0, ChatColor.GOLD + "Click to unlock for 850 BP!");
                    im.setLore(lore);
                    item.setItemMeta(im);
                    locked.add(item);
                }
                break;
            case "Wizard":
                item = new ItemStack(Material.IRON_SWORD);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "3 Kills - Melee Wizard");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Who needs wands? Get a pro sword to fight with!"));
                item.setItemMeta(im);
                unlocked.add(item);

                item = new ItemStack(Material.ENDER_PEARL);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "7 Kills - Backup Plan");
                im.setLore(Arrays.asList((ChatColor.YELLOW + "Get 10 Ender Pearls to use in case your Teleporter isn't ready.")));
                item.setItemMeta(im);
                unlocked.add(item);

                item = new ItemStack(Material.DIRT);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "13 Kills - Shockwave");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Get the powerful Shockwave wand."));
                item.setItemMeta(im);
                unlocked.add(item);

                item = new ItemStack(Material.STRING);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "25 Kills - Super Caster");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Your cooldowns are 5 seconds shorter."));
                item.setItemMeta(im);
                if (stats.kitunlocks.contains("supercaster")) {
                    unlocked.add(item);
                } else {
                    im.setDisplayName(ChatColor.RED + ChatColor.stripColor(im.getDisplayName()) + " (LOCKED)");
                    List<String> lore = im.getLore();
                    lore.add(0, ChatColor.GOLD + "Click to unlock for 150 BP!");
                    im.setLore(lore);
                    item.setItemMeta(im);
                    locked.add(item);
                }
                break;
            case "Tank":
                item = new ItemStack(Material.SUGAR);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "4 Kills - Exercise");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Wow you are not as slow anymore!"));
                item.setItemMeta(im);
                unlocked.add(item);

                item = new ItemStack(Material.COOKED_BEEF);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "9 Kills - Beefy");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Your maximum HP increases!"));
                item.setItemMeta(im);
                unlocked.add(item);

                item = new ItemStack(Material.WEB);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "19 Kills - Gravity");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Everyone around you is slowed!"));
                item.setItemMeta(im);
                unlocked.add(item);

                item = new ItemStack(Material.SLIME_BALL);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "25 Kills - Repulsion");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Whenever you are hit, the attacker will be knocked back."));
                item.setItemMeta(im);
                if (stats.kitunlocks.contains("repulsion")) {
                    unlocked.add(item);
                } else {
                    im.setDisplayName(ChatColor.RED + ChatColor.stripColor(im.getDisplayName()) + " (LOCKED)");
                    List<String> lore = im.getLore();
                    lore.add(0, ChatColor.GOLD + "Click to unlock for 350 BP!");
                    im.setLore(lore);
                    item.setItemMeta(im);
                    locked.add(item);
                }
                break;
            case "Gladiator":
                item = new ItemStack(Material.RAILS);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "3 Kills - Relentless");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Get a temporary speed boost whenever you hit an enemy!"));
                item.setItemMeta(im);
                unlocked.add(item);

                item = new ItemStack(Material.COMPASS);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "7 Kills - Bloodthirsty");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Get a magic compass to warp you to nearby players."));
                item.setItemMeta(im);
                unlocked.add(item);

                item = new ItemStack(Material.POWERED_RAIL);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "15 Kills - Unstoppable");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "You have a 30% chance to ignore damage!"));
                item.setItemMeta(im);
                unlocked.add(item);

                item = new ItemStack(Material.STRING);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "25 Kills - Duelist");
                im.setLore(Arrays.asList(ChatColor.YELLOW + "Regeneration! Yay!"));
                item.setItemMeta(im);
                if (stats.kitunlocks.contains("duelist")) {
                    unlocked.add(item);
                } else {
                    im.setDisplayName(ChatColor.RED + ChatColor.stripColor(im.getDisplayName()) + " (LOCKED)");
                    List<String> lore = im.getLore();
                    lore.add(0, ChatColor.GOLD + "Click to unlock for 275 BP!");
                    im.setLore(lore);
                    item.setItemMeta(im);
                    locked.add(item);
                }
                break;
            default:
                return;
        }
        int count = 9;
        for (ItemStack i : unlocked)
            inventory.setItem(count++, i);
        count = 36;
        for (ItemStack i : locked)
            inventory.setItem(count++, i);
        p.openInventory(inventory);
    }

    public void checkGiveKit(Player p, String s) {
        s = ChatColor.stripColor(s);
        String capitalized = Character.toUpperCase(s.charAt(0)) + s.substring(1);
        if (s.contains("mercenary")) {
            giveKit(p, "mercenary");
            p.sendMessage("");
            p.sendMessage(ChatColor.GREEN + "You are now a Mercenary!");
            p.sendMessage(ChatColor.GREEN + "Go out there and kill some noobs!");
            p.sendMessage(ChatColor.GREEN + "Left-click the " + ChatColor.AQUA + capitalized + ChatColor.GREEN + " to see your kill streak rewards!");
        } else if (s.contains("archer")) {
            giveKit(p, "archer");
            p.sendMessage("");
            p.setAllowFlight(true);
            p.sendMessage(ChatColor.GREEN + "You are now an Archer!");
            p.sendMessage(ChatColor.GREEN + "Double-tap space to double jump!");
            p.sendMessage(ChatColor.GREEN + "Left-click the " + ChatColor.AQUA + capitalized + ChatColor.GREEN + " to see your kill streak rewards!");
        } else if (s.contains("wizard")) {
            if (RonboPVP.get().getStats().containsKey(p.getName())) {
                if (RonboPVP.get().getStats().get(p.getName()).kitunlocks.contains("wizardclass") || RankManager.getRank(p).equalsIgnoreCase("helper")) {
                    giveKit(p, "wizard");
                    p.sendMessage("");
                    p.sendMessage(ChatColor.GREEN + "You are now a Wizard!");
                    p.sendMessage(ChatColor.GREEN + "Remember, right click to cast spells!");
                    p.sendMessage(ChatColor.GREEN + "Left-click the " + ChatColor.AQUA + capitalized + ChatColor.GREEN + " to see your kill streak rewards!");
                } else {
                    p.sendMessage(ChatColor.RED + "You haven't unlocked the Wizard class yet!");
                    p.sendMessage(ChatColor.RED + "Use " + ChatColor.YELLOW + "/unlock wizard" + ChatColor.RED + " to unlock Wizard for 4000 BP!");
                }
            }
        } else if (s.contains("gladiator")) {
            giveKit(p, "gladiator");
            p.sendMessage("");
            p.sendMessage(ChatColor.GREEN + "You are now a Gladiator!");
            p.sendMessage(ChatColor.GREEN + "Everyone shall fear your blade!");
            p.sendMessage(ChatColor.GREEN + "Left-click the " + ChatColor.AQUA + capitalized + ChatColor.GREEN + " to see your kill streak rewards!");
        } else if (s.contains("tank")) {
            if (RonboPVP.get().getStats().containsKey(p.getName())) {
                if (RonboPVP.get().getStats().get(p.getName()).kitunlocks.contains("tankclass") || RankManager.getRank(p).equalsIgnoreCase("helper")) {
                    giveKit(p, "tank");
                    p.sendMessage("");
                    p.addPotionEffect(PotionEffectType.SLOW.createEffect(200000, 1));
                    p.addPotionEffect(PotionEffectType.WEAKNESS.createEffect(200000, 0));
                    p.sendMessage(ChatColor.GREEN + "You are now a Tank!");
                    p.sendMessage(ChatColor.GREEN + "Don't die, it'll be embarrassing!");
                    p.sendMessage(ChatColor.GREEN + "Left-click the " + ChatColor.AQUA + capitalized + ChatColor.GREEN + " to see your kill streak rewards!");
                } else {
                    p.sendMessage(ChatColor.RED + "You haven't unlocked the Tank class yet!");
                    p.sendMessage(ChatColor.RED + "Use " + ChatColor.YELLOW + "/unlock tank" + ChatColor.RED + " to unlock Tank for 4000 BP!");
                }
            }
        }
    }

    public void giveKit(Player p, String s) {
        RonboPVP.get().getGame().checkCanCast(p, 2500);
        Utils.removeMetadata(p);
        p.setMaxHealth(20.0);
        p.getInventory().clear();
        p.setAllowFlight(false);
        RonboPVP.get().getGame().killcount.put(p.getName(), 0);
        for (PotionEffect pe : p.getActivePotionEffects())
            p.removePotionEffect(pe.getType());
        List<ItemStack> kit = RonboPVP.get().getGame().kits.get(s).getKitItems();
        p.getInventory().setHelmet(kit.get(0));
        p.getInventory().setChestplate(kit.get(1));
        p.getInventory().setLeggings(kit.get(2));
        p.getInventory().setBoots(kit.get(3));
        p.removeMetadata("class", RonboPVP.get());
        p.setMetadata("class", new FixedMetadataValue(RonboPVP.get(), s));
        for (int k = 4; k < kit.size(); k++)
            p.getInventory().addItem(kit.get(k));
        p.updateInventory();
    }
}
