package xronbo.ronbopvp.kits;

import com.google.common.collect.Lists;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;
import xronbo.ronbopvp.RonboPVP;

import java.util.Arrays;
import java.util.List;

public class MercenaryKit extends AbstractKit {

    public MercenaryKit() {
        super("Mercenary");
    }

    @Override
    public void onKillStreak(int ks, Player p, RonboPVP.Stats stats) {
        ItemStack item;
        Potion potion;
        ItemMeta im;
        switch (ks) {
            case 2:
                p.sendMessage(ChatColor.AQUA + "Mercenary " + ks + " Kill Streak Reward Earned: Well Supplied");
                potion = new Potion(PotionType.INSTANT_HEAL);
                potion.setLevel(2);
                p.getInventory().addItem(potion.toItemStack(20));
                break;
            case 5:
                item = new ItemStack(Material.BLAZE_ROD);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.GOLD + "Lightning Rod");
                im.setLore(Arrays.asList(new String[]{ChatColor.YELLOW + "Right click to summon lightning!",
                        ChatColor.YELLOW + "Bolts Left: 10"}));
                item.setItemMeta(im);
                p.sendMessage(ChatColor.AQUA + "Mercenary " + ks + " Kill Streak Reward Earned: Lightning Rod");
                p.getInventory().addItem(item);
                break;
            case 9:
                item = new ItemStack(Material.TNT);
                im = item.getItemMeta();
                im.setDisplayName(ChatColor.RED + "Explosive Rain Activator");
                im.setLore(Arrays.asList(new String[]{ChatColor.YELLOW + "Place this down to use it!",}));
                item.setItemMeta(im);
                p.sendMessage(ChatColor.AQUA + "Mercenary " + ks + " Kill Streak Reward Earned: Explosive Rain");
                p.getInventory().addItem(item);
                break;
            case 13:
                if (stats.kitunlocks.contains("wolfpack")) {
                    item = new ItemStack(Material.BONE);
                    im = item.getItemMeta();
                    im.setDisplayName(ChatColor.BLUE + "Wolf Pack Summoner");
                    im.setLore(Arrays.asList(new String[]{ChatColor.YELLOW + "Call in the wolves!!!",}));
                    item.setItemMeta(im);
                    p.sendMessage(ChatColor.AQUA + "Mercenary " + ks + " Kill Streak Reward Earned: Wolf Pack");
                    p.getInventory().addItem(item);
                }
                break;
            case 40:
                if (stats.kitunlocks.contains("superwolfpack")) {
                    item = new ItemStack(Material.BONE);
                    im = item.getItemMeta();
                    im.setDisplayName(ChatColor.BLUE + "Super Wolf Pack Summoner");
                    im.setLore(Arrays.asList(new String[]{ChatColor.YELLOW + "Call in the Super Wolfies!!!",}));
                    item.setItemMeta(im);
                    p.sendMessage(ChatColor.AQUA + "Mercenary " + ks + " Kill Streak Reward Earned: Super Wolf Pack");
                    p.getInventory().addItem(item);
                }
                break;
            default:
                break;
        }
    }

    public List<ItemStack> getKitItems() {
        List<ItemStack> kit = Lists.newArrayList();
        ItemStack item;
        ItemMeta im;
        item = new ItemStack(Material.IRON_HELMET);
        kit.add(item);
        item = new ItemStack(Material.IRON_CHESTPLATE);
        kit.add(item);
        item = new ItemStack(Material.IRON_LEGGINGS);
        kit.add(item);
        item = new ItemStack(Material.IRON_BOOTS);
        kit.add(item);
        item = new ItemStack(Material.IRON_SWORD);
        item.addEnchantment(Enchantment.DAMAGE_ALL, 1);
        im = item.getItemMeta();
        im.setDisplayName(ChatColor.DARK_GREEN + "Gravity Sword");
        im.setLore(Arrays.asList(ChatColor.YELLOW + "Right click to cast spell!"));
        item.setItemMeta(im);
        kit.add(item);
        item = new ItemStack(Material.BOW);
        item.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
        im = item.getItemMeta();
        im.setDisplayName(ChatColor.AQUA + "Just Your Average Bow");
        item.setItemMeta(im);
        kit.add(item);
        Potion potion = new Potion(PotionType.INSTANT_HEAL);
        potion.setLevel(2);
        kit.add(potion.toItemStack(5));
        item = new ItemStack(Material.COOKED_BEEF);
        item.setAmount(64);
        kit.add(item);
        item = new ItemStack(Material.ARROW);
        item.setAmount(64);
        kit.add(item);
        return kit;
    }
}
