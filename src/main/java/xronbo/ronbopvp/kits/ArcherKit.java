package xronbo.ronbopvp.kits;

import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;
import xronbo.ronbopvp.RonboPVP;

import java.util.Arrays;
import java.util.List;

public class ArcherKit extends AbstractKit {

    public ArcherKit() {
        super("Archer");
    }

    @Override
    public void onKillStreak(int ks, Player p, RonboPVP.Stats stats) {
        switch (ks) {
            case 5:
                p.sendMessage(ChatColor.AQUA + "Archer " + ks + " Kill Streak Reward Earned: Fire Spirit");
                p.setMetadata("firespirit", new FixedMetadataValue(RonboPVP.get(), 0));
                Bukkit.getScheduler().scheduleSyncDelayedTask(RonboPVP.get(), () -> p.removeMetadata("firespirit", plugin), 2 * 20 * 60);
                break;
            case 10:
                p.sendMessage(ChatColor.AQUA + "Archer " + ks + " Kill Streak Reward Earned: Thunder Spirit");
                p.setMetadata("thunderspirit", new FixedMetadataValue(plugin, 0));
                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> p.removeMetadata("thunderspirit", plugin), 2 * 20 * 60);
                break;
            case 15:
                if (stats.kitunlocks.contains("earthspirit")) {
                    p.sendMessage(ChatColor.AQUA + "Archer " + ks + " Kill Streak Reward Earned: Earth Spirit");
                    p.setMetadata("earthspirit", new FixedMetadataValue(plugin, 0));
                    plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> p.removeMetadata("earthspirit", plugin), 2 * 20 * 60);
                }
                break;
            case 25:
                if (stats.kitunlocks.contains("kastiaspirit")) {
                    p.sendMessage(ChatColor.AQUA + "Archer " + ks + " Kill Streak Reward Earned: Spirit of Kastia");
                    p.setMetadata("kastiaspirit", new FixedMetadataValue(plugin, 0));
                }
                break;
            case 35:
                if (stats.kitunlocks.contains("windspirit")) {
                    p.sendMessage(ChatColor.AQUA + "Archer " + ks + " Kill Streak Reward Earned: Wind Spirit");
                    p.setMetadata("windspirit", new FixedMetadataValue(plugin, 0));
                }
                break;
            default:
                break;
        }
    }

    public List<ItemStack> getKitItems() {
        List<ItemStack> kit = Lists.newArrayList();
        ItemStack item = new ItemStack(Material.LEATHER_HELMET);
        ItemMeta im;
        item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        kit.add(item);
        item = new ItemStack(Material.LEATHER_CHESTPLATE);
        item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        kit.add(item);
        item = new ItemStack(Material.LEATHER_LEGGINGS);
        item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        kit.add(item);
        item = new ItemStack(Material.LEATHER_BOOTS);
        item.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
        kit.add(item);
        item = new ItemStack(Material.BOW);
        im = item.getItemMeta();
        im.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
        im.setDisplayName(ChatColor.RED + "Bow of the Heavens");
        im.setLore(Arrays.asList(ChatColor.YELLOW + "Left click to cast spell!"));
        item.setItemMeta(im);
        item.addEnchantment(Enchantment.ARROW_DAMAGE, 3);
        kit.add(item);
        item = new ItemStack(Material.IRON_SWORD);
        im = item.getItemMeta();
        im.setDisplayName(ChatColor.AQUA + "Arrow Deflector");
        im.setLore(Arrays.asList(ChatColor.YELLOW + "Right click to cast spell!"));
        item.setItemMeta(im);
        kit.add(item);
        Potion potion = new Potion(PotionType.INSTANT_HEAL);
        potion.setLevel(2);
        kit.add(potion.toItemStack(5));
        item = new ItemStack(Material.COOKED_BEEF);
        item.setAmount(64);
        kit.add(item);
        item = new ItemStack(Material.ARROW);
        item.setAmount(64);
        kit.add(item);
        kit.add(item);
        kit.add(item);
        kit.add(item);
        kit.add(item);
        return kit;
    }
}
