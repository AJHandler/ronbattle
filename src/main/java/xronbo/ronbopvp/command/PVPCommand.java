package xronbo.ronbopvp.command;

import net.kastia.KastiaCore.ranks.RankManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import xronbo.ronbopvp.RonboPVP;
import xronbo.ronbopvp.utils.Utils;

import java.util.concurrent.ConcurrentHashMap;

public class PVPCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        try {
            if (sender instanceof Player) {
                final Player p = (Player) sender;
                if (cmd.getName().equalsIgnoreCase("db")) {
                    if (RankManager.getRank(p).equalsIgnoreCase("owner")) {
                        if (args.length == 1) {
                            if (args[0].equals("tornado")) {
                                Utils.createTornado(p.getLocation(), p);
                            } else if (args[0].equals("wave")) {
                                Utils.createShockwave(p);
                            } else {
                                for (int k = 0; k < Integer.parseInt(args[0]); k++)
                                    RonboPVP.get().getGame().giveKill(p.getName(), "test");
                            }
                        } else {
                            RonboPVP.get().getGame().giveKill(p.getName(), "test");
                        }
                    }
                }
                ConcurrentHashMap<String, RonboPVP.Stats> stats = RonboPVP.get().getStats();
                if (cmd.getName().equalsIgnoreCase("unlock")) {
                    if (args.length == 1) {
                        switch (args[0].toLowerCase()) {
                            case "wizard":
                                if (stats.get(p.getName()).kitunlocks.contains("wizardclass")) {
                                    p.sendMessage(ChatColor.RED + "You've already unlocked the Wizard class!");
                                } else {
                                    int price = 4000;
                                    if (stats.get(p.getName()).getTotalPoints() >= price) {
                                        if (stats.get(p.getName()).earnedPoints >= price) {
                                            stats.get(p.getName()).earnedPoints -= price;
                                        } else {
                                            price -= stats.get(p.getName()).earnedPoints;
                                            stats.get(p.getName()).earnedPoints = 0;
                                            stats.get(p.getName()).points -= price;
                                        }
                                        stats.get(p.getName()).kitunlocks.add("wizardclass");
                                        p.sendMessage(ChatColor.GREEN + "You unlocked the Wizard class! Have fun!");
                                    } else {
                                        p.sendMessage(ChatColor.RED + "You need 4000 BP to unlock the Wizard class!");
                                    }
                                }
                                break;
                            case "tank":
                                if (stats.get(p.getName()).kitunlocks.contains("tankclass")) {
                                    p.sendMessage(ChatColor.RED + "You've already unlocked the Tank class!");
                                } else {
                                    int price = 4000;
                                    if (stats.get(p.getName()).getTotalPoints() >= price) {
                                        if (stats.get(p.getName()).earnedPoints >= price) {
                                            stats.get(p.getName()).earnedPoints -= price;
                                        } else {
                                            price -= stats.get(p.getName()).earnedPoints;
                                            stats.get(p.getName()).earnedPoints = 0;
                                            stats.get(p.getName()).points -= price;
                                        }
                                        stats.get(p.getName()).kitunlocks.add("tankclass");
                                        p.sendMessage(ChatColor.GREEN + "You unlocked the Tank class! Have fun!");
                                    } else {
                                        p.sendMessage(ChatColor.RED + "You need 4000 BP to unlock the Wizard class!");
                                    }
                                }
                                break;
                        }
                    }
                }
                if (cmd.getName().equalsIgnoreCase("stats")) {
                    RonboPVP.Stats s = stats.get(p.getName());
                    if (s != null) {
                        String kdr = "";
                        if (s.deaths == 0) {
                            kdr = "N/A";
                        } else {
                            kdr = String.format("%.2f", ((double) s.kills) / s.deaths);
                        }
                        p.sendMessage(ChatColor.GRAY + "----------------------------------------------------");
                        p.sendMessage(ChatColor.GREEN + "Kills: " + s.kills + ChatColor.RED + "   Deaths: " + s.deaths + ChatColor.YELLOW + "   KDR: " + kdr);
                        p.sendMessage(ChatColor.GOLD + "Battle Points: " + s.getTotalPoints() + ChatColor.LIGHT_PURPLE + "   Longest Killstreak: " + s.longest);
                        p.sendMessage(ChatColor.AQUA + "Join Date: " + s.joinDate);
                        p.sendMessage(ChatColor.GRAY + "----------------------------------------------------");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
