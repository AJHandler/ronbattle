package xronbo.ronbopvp.arena;


import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import xronbo.ronbopvp.RonboPVP;
import xronbo.ronbopvp.utils.Cuboid;

public class ArenaManager implements Listener {

    private World world;

    private Cuboid cuboid;

    public ArenaManager(World world) {
        this.world = world;
        this.cuboid = new Cuboid(
                new Location(world, 95, 65, -10, 0, 0),
                new Location(world, 1, 100, 1 , 0, 0)
        );
        postWorldLoad();
    }

    @EventHandler
    public void onPlayerMoveEvent(PlayerMoveEvent event) {
        if (event.getTo().getWorld().equals(world)) {
            if(cuboid.hasPlayerInside(event.getPlayer())) return;
            if (!event.getPlayer().hasMetadata("exited")) {
                event.getPlayer().setMetadata("exited", new FixedMetadataValue(RonboPVP.get(), 0));
                event.getPlayer().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "You are now in the Fight Zone!");
            }
        }
    }

    private void postWorldLoad() {
        world.setGameRuleValue("doFireTick", "false");
        world.setGameRuleValue("doMobSpawning", "false");
        world.setGameRuleValue("randomTickSpeed", "0");

        Location mercenaryNPC =  new Location(world, 95, 65, -10, 0, 0);
        createNPC(mercenaryNPC, "Mercenary",Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.LEATHER_BOOTS, Material.DIAMOND_SWORD);
        createNPC(mercenaryNPC.clone().add(3, 0, 1), "Archer", Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.LEATHER_BOOTS, Material.DIAMOND_SWORD);
        createNPC(mercenaryNPC.clone().add(6, 0, 1), "Wizard", Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.LEATHER_BOOTS, Material.DIAMOND_SWORD);
        createNPC(mercenaryNPC.clone().add(9, 0, 1), "Gladiator", Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.LEATHER_BOOTS, Material.DIAMOND_SWORD);
        createNPC(mercenaryNPC.clone().add(12, 0, 1), "Tank", Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.LEATHER_BOOTS, Material.DIAMOND_SWORD);
    }

    private void createNPC(Location spawn, String name, Material...armor){
        NPC npc = CitizensAPI.getNPCRegistry().createNPC(EntityType.ZOMBIE, ChatColor.AQUA + "" + ChatColor.BOLD + name);
        npc.setFlyable(false);
        npc.setProtected(true);
        npc.spawn(spawn);
        ItemStack helmet = new ItemStack(armor[0]);
        ItemStack chestplate = new ItemStack(armor[1]);
        ItemStack leggings = new ItemStack(armor[2]);
        ItemStack boots = new ItemStack(armor[3]);
        ItemStack weapon = new ItemStack(armor[4]);
        weapon.addEnchantment(Enchantment.DAMAGE_ALL, 1);
        ItemStack[] equips = {helmet, chestplate, leggings, boots};
        LivingEntity livingEntity = (LivingEntity) npc.getEntity();
        EntityEquipment ee = livingEntity.getEquipment();
        ee.setArmorContents(equips);
        ee.setItemInHand(weapon);
        ee.setItemInHandDropChance(0);
        ee.setHelmetDropChance(0);
        ee.setChestplateDropChance(0);
        ee.setLeggingsDropChance(0);
        ee.setBootsDropChance(0);
    }

}
