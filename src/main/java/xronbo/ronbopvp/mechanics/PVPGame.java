package xronbo.ronbopvp.mechanics;

import net.kastia.KastiaCore.ranks.RankManager;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;
import xronbo.ronbopvp.RonboPVP;
import xronbo.ronbopvp.RonboPVP.Stats;
import xronbo.ronbopvp.kits.*;
import xronbo.ronbopvp.utils.Utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

public class PVPGame implements Listener {

	private Plugin plugin = RonboPVP.get();

	//Selectable Kits
	public HashMap<String, AbstractKit> kits = new HashMap<>();

	public ConcurrentHashMap<String, String> lastAttackedBy = new ConcurrentHashMap<>();

	public ConcurrentHashMap<String, Integer> killcount = new ConcurrentHashMap<>();
	public String top = null;
	public long lastCheck = 0;

	public HashMap<String, Long> doublejumpcd = new HashMap<>();

	public HashMap<String, Long> cooldowns = new HashMap<>();

	public PVPGame() {
		kits.put("archer", new ArcherKit());
		kits.put("gladiator", new GladiatorKit());
		kits.put("mercenary", new MercenaryKit());
		kits.put("tank", new TankKit());
		kits.put("wizard", new WizardKit());
	}

	@EventHandler
	public void onfire(BlockSpreadEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void quitlol(PlayerQuitEvent event) {
		killcount.remove(event.getPlayer().getName());
	}

	@EventHandler
	public void onPlayerAttack(EntityDamageByEntityEvent event) {
		if (event.getEntity() instanceof Player) {
			if (event.getDamager() instanceof Player) {
				lastAttackedBy.put(event.getEntity().getName(), event.getDamager().getName());
			} else if (event.getDamager() instanceof Projectile && ((Projectile) event.getDamager()).getShooter() instanceof Player) {
				lastAttackedBy.put(event.getEntity().getName(), ((Player) ((Projectile) event.getDamager()).getShooter()).getName());
			}
		}
	}

	@EventHandler
	public void postEnter(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		final Scoreboard board = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
		final String name = p.getName();
		p.setScoreboard(board);
		killcount.put(p.getName(), 0);
		Utils.removeMetadata(p);
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(RonboPVP.get(), new Runnable() {
			public Objective side;

			public String getTop() {
				int max = -1;
				if (System.currentTimeMillis() - lastCheck > 2500 || top == null) {
					String s = "";
					lastCheck = System.currentTimeMillis();
					for (Entry<String, Integer> e : killcount.entrySet()) {
						if (e.getValue() > max) {
							s = e.getKey();
							max = e.getValue();
						}
					}
					return top = s;
				} else {
					return top;
				}
			}

			public void run() {
				if (Bukkit.getServer().getPlayerExact(name) == null)
					return;
				if (side != null)
					side.unregister();
				String top = getTop();
				String topdisplay = ChatColor.GOLD + top;
				if (topdisplay.length() > 16)
					topdisplay = topdisplay.substring(0, 16);
				side = board.registerNewObjective("info", "dummy");
				side.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Ronbattle");
				side.setDisplaySlot(DisplaySlot.SIDEBAR);
				int count = 14;
				side.getScore(ChatColor.GOLD + "Top Streak").setScore(count--);
				side.getScore(topdisplay).setScore(count--);
				side.getScore(ChatColor.GOLD + "" + (killcount.containsKey(top) ? killcount.get(top) + " Kills" : "")).setScore(count--);
				side.getScore(ChatColor.AQUA + "").setScore(count--);
				side.getScore(ChatColor.AQUA + "Players Online").setScore(count--);
				side.getScore(ChatColor.AQUA + "" + Bukkit.getServer().getOnlinePlayers().size()).setScore(count--);
				side.getScore(ChatColor.BLACK + "").setScore(count--);
				side.getScore(ChatColor.GREEN + "Kill Streak").setScore(count--);
				side.getScore(ChatColor.GREEN + "" + (killcount.containsKey(name) ? killcount.get(name) : 0) + " Kills").setScore(count--);
				side.getScore(ChatColor.BLUE + "").setScore(count--);
				side.getScore(ChatColor.LIGHT_PURPLE + "Battle Points").setScore(count--);
				side.getScore(ChatColor.LIGHT_PURPLE + "" + RonboPVP.get().getStats().get(name).getTotalPoints() + " BP").setScore(count--);
				side.getScore(ChatColor.BOLD + "").setScore(count--);
				side.getScore(ChatColor.GOLD + "" + ChatColor.BOLD + "/stats").setScore(count--);
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(RonboPVP.get(), this, 10);
			}
		});
	}

	@EventHandler
	public void leavewolfies(PlayerQuitEvent event) {
		if (event.getPlayer().getWorld() != null)
			event.getPlayer().getWorld().getEntities().stream().filter(e -> e instanceof Wolf).filter(e -> e.getCustomName() != null &&
					e.getCustomName().contains(event.getPlayer().getName())).forEach(Entity::remove);
	}

	@EventHandler
	public void ondeath(final PlayerDeathEvent event) {
		event.getDrops().clear();
		event.getEntity().getInventory().clear();
		event.getEntity().getInventory().setArmorContents(null);
		String s = event.getEntity().getName();
		event.getEntity().getWorld().getEntities().stream().filter(e -> e instanceof Wolf).filter(e -> e.getCustomName() != null &&
				e.getCustomName().contains(event.getEntity().getName())).forEach(Entity::remove);
		for (PotionEffect pe : event.getEntity().getActivePotionEffects())
			event.getEntity().removePotionEffect(pe.getType());
		if (killcount.containsKey(s)) {
			int kills = killcount.get(s);
			if (kills >= 5) {
				if (lastAttackedBy.containsKey(s)) {
					Bukkit.broadcastMessage(ChatColor.RED + s + " was shut down by " + lastAttackedBy.get(s) + " after " + kills + " kills!");
				} else {
					Bukkit.broadcastMessage(ChatColor.RED + s + " was shut down after " + kills + " kills!");
				}
			}
		}
		if (lastAttackedBy.containsKey(s)) {
			event.getEntity().sendMessage(ChatColor.RED + "You were killed by " + lastAttackedBy.get(s) + ".");
		} else {
			event.getEntity().sendMessage(ChatColor.RED + "You were killed!");
		}
		Utils.removeMetadata(event.getEntity());
		if (lastAttackedBy.containsKey(s)) {
			String killer = lastAttackedBy.get(s);
			if (!killer.equalsIgnoreCase(s) && plugin.getServer().getPlayerExact(killer) != null) {
				giveKill(killer, s);
			}
		}
		lastAttackedBy.remove(s);
		killcount.put(s, 0);
		RonboPVP.get().getStats().get(s).deaths++;
	}

	public void giveKill(String killer, String killed) {
		if (killcount.containsKey(killer))
			killcount.put(killer, killcount.get(killer) + 1);
		else
			killcount.put(killer, 1);
		checkKillStreak(killer);
		RonboPVP.get().getStats().get(killer).kills++;
		int earned = 1;
		if (Bukkit.getServer().getPlayerExact(killer) != null && RankManager.getRank(Bukkit.getServer().getPlayerExact(killer)).equalsIgnoreCase("noble")) {
			Bukkit.getServer().getPlayerExact(killer).sendMessage(ChatColor.GREEN + "As a Noble, you earn 2x BP!");
			earned = 2;
		} else if (Bukkit.getServer().getPlayerExact(killer) != null && RankManager.getRank(Bukkit.getServer().getPlayerExact(killer)).equalsIgnoreCase("royal")) {
			Bukkit.getServer().getPlayerExact(killer).sendMessage(ChatColor.GREEN + "As a Royal, you earn 3x BP!");
			earned = 3;
		}
		RonboPVP.get().getStats().get(killer).earnedPoints += earned;
		if (killcount.get(killer) % 3 == 0) {
			Potion potion = new Potion(PotionType.INSTANT_HEAL);
			potion.setLevel(2);
			Bukkit.getServer().getPlayerExact(killer).getInventory().addItem(potion.toItemStack(5));
			Bukkit.getServer().getPlayerExact(killer).getInventory().addItem(new ItemStack(Material.COOKED_BEEF));
			Bukkit.getServer().getPlayerExact(killer).sendMessage(ChatColor.GREEN + "You get 5 potions and 1 steak for getting 3 kills!");
		}
		Bukkit.getServer().getPlayerExact(killer).sendMessage(ChatColor.GREEN + "You get " + earned + " Battle Point" + (earned > 1 ? "s" : "")
				+ " for killing " + killed + "! (Total: " + RonboPVP.get().getStats().get(killer).getTotalPoints() + ")");
		if (killcount.get(killer) > RonboPVP.get().getStats().get(killer).longest) {
			RonboPVP.get().getStats().get(killer).longest = killcount.get(killer);
			Bukkit.getServer().getPlayerExact(killer).sendMessage(ChatColor.GREEN + "You reached a new longest killstreak of " +
					RonboPVP.get().getStats().get(killer).longest + " kill" + (RonboPVP.get().getStats().get(killer).longest > 1 ? "s" : "") + "!");
		}
		if (killcount.get(killer) % 5 == 0) {
			switch (killcount.get(killer)) {
				case 0:
					break;
				case 5:
					Bukkit.broadcastMessage(ChatColor.GOLD + killer + " is on a 5 kill streak!");
					break;
				case 10:
					Bukkit.broadcastMessage(ChatColor.GOLD + killer + " is on a 10 kill streak! So pro!");
					break;
				case 15:
					Bukkit.broadcastMessage(ChatColor.GOLD + killer + " is on a 15 kill streak! Better go kill'em!");
					break;
				case 20:
					Bukkit.broadcastMessage(ChatColor.GOLD + killer + " is on a 20 kill streak! So many kill streak rewards...");
					break;
				case 25:
					Bukkit.broadcastMessage(ChatColor.GOLD + killer + " is on a 25 kill streak! Watch out!");
					break;
				case 30:
					Bukkit.broadcastMessage(ChatColor.GOLD + killer + " is on a 30 kill streak! So OP!");
					break;
				case 35:
					Bukkit.broadcastMessage(ChatColor.GOLD + killer + " is on a 35 kill streak! Unstoppable??");
					break;
				case 40:
					Bukkit.broadcastMessage(ChatColor.GOLD + killer + " is on a 40 kill streak! Godly PVPer!");
					break;
				default:
					Bukkit.broadcastMessage(ChatColor.GOLD + killer + " is on a " + killcount.get(killer) + " kill streak! Goodness gracious!");
					break;
			}
		}
	}

	public void checkKillStreak(String s) {
		Potion potion = null;
		ItemStack item = null;
		ItemMeta im = null;
		if (killcount.containsKey(s) && plugin.getServer().getPlayerExact(s) != null) {
			final Player p = plugin.getServer().getPlayerExact(s);
			int ks = killcount.get(s);
			Stats stats = RonboPVP.get().getStats().get(p.getName());
			switch (ks) {
				case 5:
					p.sendMessage(ChatColor.AQUA + "" + ks + " Kill Streak Reward Earned: Speed I");
					p.removePotionEffect(PotionEffectType.SPEED);
					p.addPotionEffect(PotionEffectType.SPEED.createEffect(Integer.MAX_VALUE, 0));
					break;
				case 10:
					p.sendMessage(ChatColor.AQUA + "" + ks + " Kill Streak Reward Earned: Jump I");
					p.removePotionEffect(PotionEffectType.JUMP);
					p.addPotionEffect(PotionEffectType.JUMP.createEffect(Integer.MAX_VALUE, 0));
					break;
				case 15:
					p.sendMessage(ChatColor.AQUA + "" + ks + " Kill Streak Reward Earned: Speed II");
					p.removePotionEffect(PotionEffectType.SPEED);
					p.addPotionEffect(PotionEffectType.SPEED.createEffect(Integer.MAX_VALUE, 1));
					if (!(p.hasMetadata("class") && p.getMetadata("class").get(0).asString().equals("tank"))) {
						p.sendMessage(ChatColor.AQUA + "" + ks + " Kill Streak Reward Earned: Bonus Health");
						p.setMaxHealth(24.0);
						p.setHealth(24.0);
					}
					break;
				case 20:
					p.sendMessage(ChatColor.AQUA + "" + ks + " Kill Streak Reward Earned: Potion Set");
					potion = new Potion(PotionType.INSTANT_HEAL);
					potion.setLevel(2);
					p.getInventory().addItem(potion.toItemStack(10));
					potion = new Potion(PotionType.FIRE_RESISTANCE);
					potion.setLevel(1);
					p.getInventory().addItem(potion.toItemStack(2));
					potion = new Potion(PotionType.WATER_BREATHING);
					potion.setLevel(1);
					p.getInventory().addItem(potion.toItemStack(2));
					potion = new Potion(PotionType.INSTANT_DAMAGE);
					potion.setLevel(1);
					potion.setSplash(true);
					p.getInventory().addItem(potion.toItemStack(2));
					break;
				case 25:
					p.sendMessage(ChatColor.AQUA + "" + ks + " Kill Streak Reward Earned: Speed III");
					p.removePotionEffect(PotionEffectType.SPEED);
					p.addPotionEffect(PotionEffectType.SPEED.createEffect(Integer.MAX_VALUE, 2));
					if (!(p.hasMetadata("class") && p.getMetadata("class").get(0).asString().equals("tank"))) {
						p.sendMessage(ChatColor.AQUA + "" + ks + " Kill Streak Reward Earned: Bonus Health");
						p.setMaxHealth(30.0);
						p.setHealth(30.0);
					}
					break;
				case 30:
					p.sendMessage(ChatColor.AQUA + "" + ks + " Kill Streak Reward Earned: Strength I (Temporary)");
					p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
					p.addPotionEffect(PotionEffectType.INCREASE_DAMAGE.createEffect(20 * 60, 0));
					break;
				case 35:
					p.sendMessage(ChatColor.AQUA + "" + ks + " Kill Streak Reward Earned: Strength I (Temporary)");
					p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
					p.addPotionEffect(PotionEffectType.INCREASE_DAMAGE.createEffect(20 * 60, 0));
					break;
				case 40:
					p.sendMessage(ChatColor.AQUA + "" + ks + " Kill Streak Reward Earned: Strength II (Temporary)");
					p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
					p.addPotionEffect(PotionEffectType.INCREASE_DAMAGE.createEffect(20 * 60, 1));
					break;
				case 45:
					p.sendMessage(ChatColor.AQUA + "" + ks + " Kill Streak Reward Earned: Strength II (Temporary)");
					p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
					p.addPotionEffect(PotionEffectType.INCREASE_DAMAGE.createEffect(20 * 60, 1));
					break;
				case 50:
					if (!(p.hasMetadata("class") && p.getMetadata("class").get(0).asString().equals("tank"))) {
						p.sendMessage(ChatColor.AQUA + "" + ks + " Kill Streak Reward Earned: Bonus Health");
						p.setMaxHealth(40.0);
						p.setHealth(40.0);
					}
					break;
				default:
					break;
			}
			if (p.hasMetadata("class")) {
				String className = p.getMetadata("class").get(0).asString();
				if (kits.get(className) != null) kits.get(className).onKillStreak(ks, p, stats);
			}
		}
	}

	@EventHandler
	public void onInvOpenFurnace(PlayerInteractEvent event) {
		if (event.getClickedBlock().getType() == Material.FURNACE)
			event.setCancelled(true);
	}

	@EventHandler
	public void onProjectileLaunch(ProjectileLaunchEvent event) {
		if (event.getEntity() instanceof Arrow && event.getEntity().getShooter() instanceof Entity) {
			if (event.getEntity().getShooter() == null)
				return;
			Entity shooter = (Entity) event.getEntity().getShooter();
			if (shooter.hasMetadata("firespirit"))
				event.getEntity().setFireTicks(20 * 20);
			if (shooter.hasMetadata("thunderspirit"))
				event.getEntity().setMetadata("thunderspirit", new FixedMetadataValue(plugin, 0));
			if (shooter.hasMetadata("earthspirit"))
				event.getEntity().setMetadata("earthspirit", new FixedMetadataValue(plugin, 0));
			if (shooter.hasMetadata("kastiaspirit")) {
				event.getEntity().setFireTicks(20 * 20);
				event.getEntity().setMetadata("earthspirit", new FixedMetadataValue(plugin, 0));
			}
		}
	}

	@EventHandler
	public void onProjHit(ProjectileHitEvent event) {
		if (event.getEntity() instanceof Arrow) {
			if (event.getEntity().hasMetadata("thunderspirit"))
				event.getEntity().getWorld().strikeLightning(event.getEntity().getLocation());
			if (event.getEntity().hasMetadata("earthspirit"))
				event.getEntity().getWorld().createExplosion(event.getEntity().getLocation().getX(),
						event.getEntity().getLocation().getY(), event.getEntity().getLocation().getZ(), 1.3f, false, false);
		}
	}

	@EventHandler
	public void ondmgnotexit(EntityDamageEvent event) {
		if (!event.getEntity().hasMetadata("exited")) event.setCancelled(true);
	}

	@EventHandler
	public void ondmgbyexited(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player && !event.getDamager().hasMetadata("exited")) event.setCancelled(true);
		if (!(event.getDamager() instanceof Projectile) || !(((Projectile) event.getDamager()).getShooter() == null))
			return;
		if (event.getDamager() instanceof Projectile && !event.getDamager().hasMetadata("exited"))
			event.setCancelled(true);
	}

	@EventHandler
	public void onEntityExplode(EntityExplodeEvent event) {
		event.blockList().clear();
	}

	@EventHandler
	public void ondam124(EntityDamageByEntityEvent event) {
		if (event.getEntity().hasMetadata("repulsion"))
			event.getDamager().setVelocity(event.getEntity().getLocation().subtract(event.getDamager()
					.getLocation()).toVector().normalize().multiply(-2.0));
		if (event.getDamager().hasMetadata("relentless") && event.getDamager() instanceof LivingEntity)
			((LivingEntity) event.getDamager()).addPotionEffect(PotionEffectType.SPEED.createEffect(20 * 2, 2));
		if (event.getEntity().hasMetadata("unstoppable"))
			if (Math.random() < 0.3) event.setCancelled(true);
	}

	@EventHandler
	public void onBlockPlace(final BlockPlaceEvent event) {
		if (event.getBlock().getType() == Material.TNT) {
			if (checkCanCast(event.getPlayer(), 1000)) {
				plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
					public void run() {
						int i = event.getPlayer().getInventory().first(Material.TNT);
						if (i >= 0) {
							ItemStack item = event.getPlayer().getInventory().getItem(i);
							if (item.getAmount() > 1)
								item.setAmount(item.getAmount() - 1);
							else
								event.getPlayer().getInventory().setItem(i, new ItemStack(Material.AIR));
							event.getPlayer().getNearbyEntities(25, 50, 25).stream().filter(e -> e instanceof Player).forEach(e ->
									e.sendMessage(ChatColor.RED + "WARNING! " + event.getPlayer().getName() + " just triggered an Explosive Rain near you!"));
							event.getPlayer().sendMessage(ChatColor.RED + "You triggered an Explosive Rain. RUN. IT CAN HURT YOU TOO.");
							final Location loc = event.getPlayer().getLocation();
							plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
								public int count = 25;

								public void run() {
									for (int dx = -7; dx <= 7; dx++) {
										for (int dz = -7; dz <= 7; dz++) {
											if (count <= 0)
												break;
											if (Math.random() < 0.05) {
												count--;
												Location temp = loc.clone().add(dx, (int) (Math.random() * 15 + 10), dz);
												TNTPrimed ptnt = (TNTPrimed) temp.getWorld().spawnEntity(temp, EntityType.PRIMED_TNT);
												ptnt.setFuseTicks((int) (Math.random() * 40 + 30));
											}
										}
										if (count <= 0)
											break;
									}
									if (count > 0)
										plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, this, (int) (Math.random() * 20 + 5));
								}
							}, 60);
						}
					}
				}, 5);
			}
		}
	}

	@EventHandler
	public void fixdurability(EntityDamageEvent event) {
		if (event.getEntity() instanceof Player) {
			Player p = (Player) event.getEntity();
			if (p.getItemInHand().getType() != Material.POTION && p.getItemInHand().getType() != Material.GLASS_BOTTLE)
				p.getItemInHand().setDurability((short) 0);
			p.getEquipment().getHelmet().setDurability((short) 0);
			p.getEquipment().getChestplate().setDurability((short) 0);
			p.getEquipment().getLeggings().setDurability((short) 0);
			p.getEquipment().getBoots().setDurability((short) 0);
		}
	}

	@EventHandler
	public void onFlyToggle(PlayerToggleFlightEvent event) {
		if (event.getPlayer().hasMetadata("class") && event.getPlayer().getMetadata("class").get(0).asString().equals("archer")) {
			event.setCancelled(true);
			Location loc = event.getPlayer().getLocation();
			event.getPlayer().setVelocity(new Vector(0, 0, 0));
			event.getPlayer().setFlying(false);
			event.getPlayer().setVelocity(new Vector(0, 0, 0));
			if (doublejumpcd.containsKey(event.getPlayer().getName())) {
				if (System.currentTimeMillis() < doublejumpcd.get(event.getPlayer().getName())) {
					event.getPlayer().sendMessage(ChatColor.RED + "You can't Double Jump for " + String.format("%.1f",
							(doublejumpcd.get(event.getPlayer().getName()) - System.currentTimeMillis()) / 1000.0) + " more seconds.");
					event.getPlayer().setVelocity(new Vector(0, 0, 0));
					event.getPlayer().teleport(loc);
					return;
				}
			}
			if (event.getPlayer().hasMetadata("windspirit")) {
				doublejumpcd.put(event.getPlayer().getName(), System.currentTimeMillis() + 2500);
			} else {
				doublejumpcd.put(event.getPlayer().getName(), System.currentTimeMillis() + 5000);
			}
			event.getPlayer().setVelocity(event.getPlayer().getEyeLocation().getDirection().normalize().add(new Vector(0, 0.2, 0)));
		}
	}

	@EventHandler
	public void oniclick(InventoryClickEvent event) {
		if (event.getInventory().getName().contains("Streak Rewards")) {
			String classname = ChatColor.stripColor(event.getInventory().getName());
			classname = classname.substring(0, classname.indexOf(" Streak Rewards"));
			event.setCancelled(true);
			ItemStack item = event.getCurrentItem();
			ItemMeta im = item.getItemMeta();
			if (im.getLore().get(0).contains("Click to unlock")) {
				int price = Integer.parseInt(ChatColor.stripColor(im.getLore().get(0)).replaceAll("[^0-9]", ""));
				Stats stats = RonboPVP.get().getStats().get(event.getWhoClicked().getName());
				String tag = "";
				if (im.getDisplayName().toLowerCase().contains("wolf pack")) {
					tag = "wolfpack";
				}
				if (im.getDisplayName().toLowerCase().contains("super wolf pack")) {
					tag = "superwolfpack";
				}
				if (im.getDisplayName().toLowerCase().contains("earth spirit")) {
					tag = "earthspirit";
				}
				if (im.getDisplayName().toLowerCase().contains("spirit of kastia")) {
					tag = "kastiaspirit";
				}
				if (im.getDisplayName().toLowerCase().contains("super caster")) {
					tag = "supercaster";
				}
				if (im.getDisplayName().toLowerCase().contains("wind spirit")) {
					tag = "windspirit";
				}
				if (im.getDisplayName().toLowerCase().contains("repulsion")) {
					tag = "repulsion";
				}
				if (im.getDisplayName().toLowerCase().contains("duelist")) {
					tag = "duelist";
				}
				if (tag.length() > 0) {
					event.getWhoClicked().closeInventory();
					if (stats.getTotalPoints() >= price) {
						stats.kitunlocks.add(tag);
						stats.earnedPoints -= price;
						event.getWhoClicked().sendMessage(ChatColor.GREEN + "You unlocked " + im.getDisplayName().substring(0, im.getDisplayName().indexOf("(LOCKED)") - 1) + ChatColor.GREEN + " for " + classname + "!");
						event.getWhoClicked().sendMessage(ChatColor.GREEN + "You have " + stats.getTotalPoints() + " BP left.");
						stats.save();
					} else {
						event.getWhoClicked().sendMessage(ChatColor.RED + "You need " + price + " BP to unlock that! You have " + stats.getTotalPoints() + " BP.");
					}
				}
			}
		}
	}

	@EventHandler
	public void onDropItem(PlayerDropItemEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void oncombust(EntityCombustEvent event) {
		if (event.getEntity().hasMetadata("invincible")) {
			event.setDuration(0);
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onpie(PlayerInteractEvent event) {
		ItemStack item = event.getPlayer().getItemInHand();
		Player p = event.getPlayer();
		if (!p.hasMetadata("exited")) {
			p.sendMessage(ChatColor.RED + "You can't cast spells in spawn!");
			return;
		}
		if (item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().getDisplayName().contains("Gravity Sword")) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (checkCanCast(p, 20000)) {
					p.sendMessage(ChatColor.GOLD + "You use the Gravity Sword to draw enemies towards you.");
					for (Entity e : p.getNearbyEntities(12, 5, 12)) {
						if (e instanceof LivingEntity && !e.hasMetadata("invincible")) {
							if (e instanceof Player && !e.hasMetadata("exited"))
								continue;
							e.setVelocity(p.getLocation().subtract(e.getLocation()).toVector().normalize().multiply(1.5));
						}
					}
				}
			}
		} else if (item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().getDisplayName().contains("Lightning Rod")) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				BlockIterator bi = new BlockIterator(event.getPlayer(), 100);
				Block b = null;
				while (bi.hasNext()) {
					b = bi.next();
					if (b.getType() != Material.AIR)
						break;
				}
				if (b == null || b.getType() == Material.AIR) {
					event.getPlayer().sendMessage(ChatColor.RED + "There was no block in range (100), so your Lightning Rod didn't cast.");
				} else {
					if (checkCanCast(p, 2000)) {
						int shots = Integer.parseInt(ChatColor.stripColor(item.getItemMeta().getLore().toString()).replaceAll("[^0-9]", ""));
						if (--shots == 0) {
							event.getPlayer().sendMessage(ChatColor.RED + "Your Lightning Rod ran out of energy!");
							event.getPlayer().setItemInHand(new ItemStack(Material.AIR));
						} else {
							event.getPlayer().sendMessage(ChatColor.RED + "Your Lightning Rod can summon " + shots + " more lightning bolts!");
							ItemMeta im = item.getItemMeta();
							im.setLore(Arrays.asList(ChatColor.YELLOW + "Right click to summon lightning!",
									ChatColor.YELLOW + "Bolts Left: " + shots));
							item.setItemMeta(im);
						}
						Location loc = b.getLocation();
						loc.getWorld().strikeLightning(loc);
					}
				}
			}
		} else if (item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().getDisplayName().contains("Wolf Pack")
				&& !item.getItemMeta().getDisplayName().contains("Super")) {
			event.getPlayer().setItemInHand(new ItemStack(Material.AIR));
			Location loc = event.getPlayer().getLocation();
			int num = (int) (Math.random() * 2 + 3);
			for (int k = 0; k < num; k++) {
				Wolf w = (Wolf) event.getPlayer().getWorld().spawnEntity(loc.clone().add(Math.random() * 7 - 3, 0, Math.random() * 7 - 3), EntityType.WOLF);
				w.setOwner(event.getPlayer());
				w.setAdult();
				w.setCollarColor(DyeColor.ORANGE);
				w.setAngry(true);
				w.setCustomNameVisible(true);
				w.setCustomName(ChatColor.RED + event.getPlayer().getName() + "'s Wolfie");
				w.setMaxHealth(5.0);
				w.setHealth(5.0);
				w.addPotionEffect(PotionEffectType.SPEED.createEffect(200000, 0));
			}
			event.getPlayer().sendMessage(ChatColor.GREEN + "You summon your lovely wolf friends!");
		} else if (item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().getDisplayName().contains("Super Wolf Pack")) {
			event.getPlayer().setItemInHand(new ItemStack(Material.AIR));
			Location loc = event.getPlayer().getLocation();
			int num = (int) (Math.random() * 1 + 2);
			for (int k = 0; k < num; k++) {
				Wolf w = (Wolf) event.getPlayer().getWorld().spawnEntity(loc.clone().add(Math.random() * 7 - 3, 0, Math.random() * 7 - 3), EntityType.WOLF);
				w.setOwner(event.getPlayer());
				w.setAdult();
				w.setCollarColor(DyeColor.ORANGE);
				w.setAngry(true);
				w.setCustomNameVisible(true);
				w.setCustomName(ChatColor.RED + "" + ChatColor.BOLD + event.getPlayer().getName() + "'s Super Wolfie");
				w.setMaxHealth(12.0);
				w.setHealth(12.0);
				w.addPotionEffect(PotionEffectType.SPEED.createEffect(200000, 1));
				w.addPotionEffect(PotionEffectType.INCREASE_DAMAGE.createEffect(200000, 0));
			}
			event.getPlayer().sendMessage(ChatColor.GREEN + "You summon your lovely wolf friends!");
		} else if (item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().getDisplayName().contains("Bow of the Heavens")) {
			if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
				if (checkCanCast(event.getPlayer(), 10000)) {
					final Location loc = event.getPlayer().getTargetBlock(new HashSet<Byte>(), 0).getLocation();
					final Player shooter = event.getPlayer();
					shooter.sendMessage(ChatColor.GREEN + "A shower of arrows rains down from the heavens.");
					plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						public int count = 30;

						public void run() {
							for (int x = -3; x <= 3; x++) {
								if (count <= 0)
									break;
								for (int z = -3; z <= 3; z++) {
									if (count <= 0)
										break;
									if (Math.random() < 0.05) {
										count--;
										Location currentLoc = loc.clone();
										currentLoc.setX(loc.getX() + x - 1);
										currentLoc.setZ(loc.getZ() + z);
										currentLoc.setY(loc.getY() + 6);
										Projectile proj = ((Projectile) (loc.getWorld().spawnEntity(currentLoc, EntityType.ARROW)));
										Location loc2 = loc.clone();
										loc2.setX(loc.getX() + x);
										loc2.setZ(loc.getZ() + z);
										Vector v = loc2.toVector().subtract(currentLoc.toVector()).normalize();
										proj.setVelocity(v);
										proj.setShooter(shooter);
									}
								}
							}
							if (count > 0)
								plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, this, (int) (Math.random() * 3 + 1));
						}
					});

				}
			}
		} else if (item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().getDisplayName().contains("Arrow Deflector")) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (checkCanCast(event.getPlayer(), 20000)) {
					event.getPlayer().sendMessage(ChatColor.GREEN + "You activate your Arrow Deflector!");
					event.getPlayer().sendMessage(ChatColor.GREEN + "For 5 seconds, arrows cannot hit you.");
					event.getPlayer().setMetadata("arrowblock", new FixedMetadataValue(plugin, 1));
					final Player p2 = event.getPlayer();
					plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> p2.removeMetadata("arrowblock", plugin), 20 * 5);
				}
			}
		} else if (item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().getDisplayName().contains("Tornado")) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				BlockIterator bi = new BlockIterator(event.getPlayer(), 20);
				Block b = null;
				while (bi.hasNext()) {
					b = bi.next();
					if (b.getType() != Material.AIR)
						break;
				}
				if (b == null || b.getType() == Material.AIR) {
					event.getPlayer().sendMessage(ChatColor.RED + "There was no block in range (20), so your Tornado didn't cast.");
				} else {
					if (checkCanCast(p, 25000)) {
						Utils.createTornado(b.getLocation().add(0, 1, 0), p);
					}
				}
			}
		} else if (item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().getDisplayName().contains("Teleporter")) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (p.hasMetadata("cantp")) {
					BlockIterator bi = new BlockIterator(event.getPlayer(), 40);
					Block b = null;
					while (bi.hasNext()) {
						b = bi.next();
						if (b.getType() != Material.AIR)
							break;
					}
					if (b == null || b.getType() == Material.AIR) {
						event.getPlayer().sendMessage(ChatColor.RED + "There was no block in range (40) so you didn't Teleport.");
					} else {
						p.removeMetadata("cantp", plugin);
						p.setFallDistance(0);
						Location loc = b.getLocation().add(0, 1, 0);
						loc.setPitch(p.getLocation().getPitch());
						loc.setYaw(p.getLocation().getYaw());
						p.teleport(loc);
					}
				} else {
					if (checkCanCast(p, 20000)) {
						p.setVelocity(new Vector(0, 2, 0));
						p.sendMessage(ChatColor.GOLD + "Right click a destination within 40 blocks to teleport!");
						p.setMetadata("cantp", new FixedMetadataValue(plugin, 1));
						p.setMetadata("nofall", new FixedMetadataValue(plugin, 1));
					}
				}
			}
		} else if (item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().getDisplayName().contains("Shockwave")) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (checkCanCast(p, 15000)) {
					p.sendMessage(ChatColor.GOLD + "Boom! A powerful shockwave ripples across the ground.");
					Utils.createShockwave(p);
				}
			}
		} else if (item.hasItemMeta() && item.getItemMeta().hasDisplayName() && item.getItemMeta().getDisplayName().contains("The Hunter")) {
			if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (checkCanCast(p, 15000)) {
					Entity closest = null;
					double distance = Integer.MAX_VALUE;
					for (Entity e : p.getNearbyEntities(50, 20, 50)) {
						if (!(e instanceof Player))
							continue;
						if (e.getLocation().distanceSquared(p.getLocation()) < distance) {
							distance = e.getLocation().distanceSquared(p.getLocation());
							closest = e;
						}
					}
					if (closest != null) {
						p.sendMessage(ChatColor.GOLD + "Poof! You teleported to the closest nearby player.");
						p.teleport(closest.getLocation());
					} else {
						p.sendMessage(ChatColor.RED + "There were no players within range, so your teleport failed.");
					}

				}
			}
		}
	}

	@EventHandler
	public void ond241(EntityDamageEvent event) {
		if (event.getCause() == DamageCause.FALL) {
			if (event.getEntity().hasMetadata("nofall")) {
				event.setCancelled(true);
				event.getEntity().removeMetadata("nofall", plugin);
			}
		}
	}

	@EventHandler
	public void fallblock(EntityChangeBlockEvent event) {
		event.setCancelled(true);
	}

	@EventHandler
	public void onarrowhit(EntityDamageByEntityEvent event) {
		if (event.getEntity().hasMetadata("arrowblock") && event.getDamager() instanceof Projectile) {
			final Entity e = event.getEntity();
			event.setCancelled(true);
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> e.removeMetadata("arrowblock", plugin), 20 * 5);
		}
	}

	public boolean checkCanCast(Player p, long cooldown) {
		String s = p.getName();
		if (cooldowns.containsKey(s)) {
			if (System.currentTimeMillis() < cooldowns.get(s)) {
				p.sendMessage(ChatColor.RED + "You cannot cast any more spells for " + String.format("%.1f", (cooldowns.get(s) - System.currentTimeMillis()) / 1000.0) + " seconds.");
				return false;
			}
		}
		if (p.hasMetadata("supercaster"))
			cooldown -= 5000;
		cooldowns.put(s, System.currentTimeMillis() + cooldown);
		return true;
	}

	@EventHandler
	public void onConsume(PlayerItemConsumeEvent event) {
		if (event.getItem().getAmount() > 0) {
			if (event.getItem().getType() == Material.POTION || event.getItem().getType() == Material.GLASS_BOTTLE) {
				Potion potion = new Potion(PotionType.INSTANT_HEAL);
				potion.setLevel(2);
				event.getPlayer().getInventory().removeItem(event.getItem());
				event.getPlayer().getInventory().addItem(potion.toItemStack(event.getItem().getAmount()));
			}
		}
	}
}
